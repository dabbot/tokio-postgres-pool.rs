# tokio-postgres-pool

`tokio-postgres-pool` is a simple additive connection pool for
[`tokio-postgres`].

It creates an initial pool of connections and additively creates new
connections when the pool is either insufficient or dries up due to lost
connections.

### Installation

Add the following to your `Cargo.toml`:

```toml
[dependencies]
tokio-postgres-pool = { git = "https://github.com/zeyla/tokio-postgres-pool.rs" }
```

and then the following to your `main.rs` or `lib.rs`:

```rust,ignore
extern crate tokio_postgres_pool;
```

### License

[ISC][license].

[`tokio-postgres`]: https://crates.io/crates/tokio-postgres
[license]: https://github.com/zeyla/tokio-postgres-pool.rs/blob/master/LICENSE.md
