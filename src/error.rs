use futures::sync::{
    mpsc::SendError,
    oneshot::Canceled,
};
use postgres_shared::error::Error as PostgresSharedError;
use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
};
use super::Message;
use tokio_postgres::error::Error as PostgresError;

#[derive(Debug)]
pub enum Error {
    Canceled(Canceled),
    Postgres(PostgresError),
    PostgresShared(PostgresSharedError),
    Unknown,
    SendError,
    #[doc(hidden)]
    _Nonexhaustive,
}

impl From<Canceled> for Error {
    fn from(err: Canceled) -> Self {
        Error::Canceled(err)
    }
}

impl From<PostgresError> for Error {
    fn from(err: PostgresError) -> Self {
        Error::Postgres(err)
    }
}

impl From<PostgresSharedError> for Error {
    fn from(err: PostgresSharedError) -> Self {
        Error::PostgresShared(err)
    }
}

impl From<SendError<Message>> for Error {
    fn from(_: SendError<Message>) -> Self {
        Error::SendError
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        f.write_str(self.description())
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        match *self {
            Error::Canceled(ref inner) => inner.description(),
            Error::Postgres(ref inner) => inner.description(),
            Error::PostgresShared(ref inner) => inner.description(),
            Error::SendError => "Error sending message to inner loop",
            Error::Unknown => "This should never occur",
            Error::_Nonexhaustive => "This should really never occur",
        }
    }
}
