//! `tokio-postgres-pool` is a simple additive connection pool for
//! `tokio-postgres`.
//!
//! It creates an initial pool of connections and additively creates new
//! connections when the pool is either insufficient or dries up due to lost
//! connections.
//!
//! ### Installation
//!
//! Add the following to your `Cargo.toml`:
//!
//! ```toml
//! [dependencies]
//! tokio-postgres-pool = { git = "https://github.com/zeyla/tokio-postgres-pool.rs" }
//! ```
//!
//! and then the following to your `main.rs` or `lib.rs`:
//!
//! ```rust,ignore
//! extern crate tokio_postgres_pool;
//! ```

pub extern crate postgres_shared;
pub extern crate tokio_postgres;

extern crate futures;
extern crate tokio;

mod error;

pub use self::error::Error;

use futures::{
    future::{self, Future, IntoFuture},
    stream::Stream,
    sync::{
        mpsc::{self, UnboundedReceiver, UnboundedSender},
        oneshot::{self, Sender as OneshotSender},
    },
};
use std::{
    collections::VecDeque,
    fmt::{Debug, Formatter, Result as FmtResult},
};
use tokio_postgres::{
    params::ConnectParams,
    Client,
    Connection,
    TlsMode as PostgresTlsMode,
};

enum Message {
    Acquire(OneshotSender<Client>),
    Give(Client),
}

struct InnerConnectionPool {
    clients: VecDeque<Client>,
    queue: VecDeque<OneshotSender<Client>>,
    rx: UnboundedReceiver<Message>,
}

fn background(
    inner: InnerConnectionPool,
) -> Box<Future<Item = (), Error = Error> + Send> {
    let InnerConnectionPool {
        mut clients,
        mut queue,
        rx,
    } = inner;

    let done: Box<Future<Item = (), Error = Error> + Send> = Box::new(rx.for_each(move |msg| {
        match msg {
            Message::Acquire(tx) => {
                if let Some(client) = clients.pop_front() {
                    let _ = tx.send(client);

                    return Ok(());
                }

                queue.push_back(tx);
            },
            Message::Give(client) => {
                if let Some(tx) = queue.pop_front() {
                    let _ = tx.send(client);

                    return Ok(());
                }

                clients.push_back(client);
            },
        }

        Ok(())
    }).into_future().map_err(|_| Error::Unknown));

    done
}

/// Container pool for connections.
///
/// Connections are acquired via [`acquire`] and _should_ be pushed back to the
/// pool via [`give`]. This ensures that the pool does not run dry quickly, and
/// instead will only be replenished when connections disconnect.
///
/// When all of the connections in the pool are in use, `acquire`s will wait
/// until one is available.
///
/// [`acquire`]: #method.acquire
/// [`give`]: #method.give
pub struct ConnectionPool {
    tx: UnboundedSender<Message>,
}

impl ConnectionPool {
    /// Spawns a new connection pool, given the address to the postgres server,
    /// a handle to a tokio core, and the maximum number of connections that the
    /// pool can spawn.
    ///
    /// # Errors
    ///
    /// Resolves to an error if any of the connections failed to instantiate.
    /// This is likely an indicator of pertinent issues.
    pub fn spawn(
        params: ConnectParams,
        count: usize,
    ) -> impl Future<Item = (
        Self,
        Vec<Box<Future<Item = (), Error = Error> + Send + 'static>>,
    ), Error = Error> {
        let mut futures = vec![];

        for _ in 0..count {
            futures.push(connect(params.clone()));
        }

        future::join_all(futures).map(move |pairs| {
            let mut clients = VecDeque::new();
            let mut conns: Vec<Box<Future<Item = (), Error = Error> + Send>> = {
                Vec::with_capacity(count + 1)
            };

            for (client, conn) in pairs {
                clients.push_back(client);
                conns.push(Box::new(conn.map(|_| ()).from_err()));
            }

            let (tx, rx) = mpsc::unbounded();
            let background_future = background(InnerConnectionPool {
                queue: VecDeque::new(),
                rx,
                clients,
            });
            conns.push(Box::new(background_future));

            (Self {
                tx,
            }, conns)
        })
    }

    /// Acquires a new connection, spawning a new one if the pool does not have
    /// one available.
    ///
    /// If a connection is not immediately available, then a Future will be
    /// returned that will eventually resolve to one when a connection is
    /// [`give`]nback to the pool and available.
    ///
    /// [`give`]: #method.give
    pub fn acquire(
        &self,
    ) -> Box<Future<Item = Client, Error = Error> + Send> {
        let (tx, rx) = oneshot::channel();

        if let Err(why) = self.tx.unbounded_send(Message::Acquire(tx)) {
            return Box::new(future::err(From::from(why)));
        }

        Box::new(rx.map(|x| x).from_err())
    }

    /// Gives a connection back to the pool, pushing it to the back.
    ///
    /// If a Future is queued for a connection via [`acquire`] when one wasn't
    /// available, this will resolve the Future with the given connection.
    ///
    /// [`acquire`]: #method.acquire
    pub fn give(&self, acquire: Client) {
        let _ = self.tx.unbounded_send(Message::Give(acquire));
    }
}

impl Debug for ConnectionPool {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        f.write_str("ConnectionPool {{ tx: UnboundedSender }}")
    }
}

fn connect(
    params: ConnectParams,
) -> impl Future<Item = (Client, Connection), Error = Error> {
    tokio_postgres::connect(params, PostgresTlsMode::None).from_err()
}
